#include "FONT.h"


void BlitChar(char c, uint32_t* fbuffer, uint32_t fw, uint32_t x, uint32_t y, uint32_t texcolor){
	
	uint8_t i = 0, j = 0;
	uint8_t row;
	uint32_t offs = 0;
	
	for(j=0;j<8;j++){
		
		offs = (c%16) + ((j<<4)+((c&0xF0)<<3));
		row = bitfont[offs];
		
		for(i=0;i<8;i++){
			if((row>>(7-i)) & 0x01)
				fbuffer[(x+i)+(y+j)*fw] = texcolor;
		}
	}

}

void BlitText(uint32_t* fbuffer, uint32_t fw, char* text, uint32_t x, uint32_t y, uint32_t texcolor){
	
	uint32_t i = 0;
	uint32_t slen = strlen(text);
	
	for(i=0;i<slen;i++){

			BlitChar(text[i], fbuffer, fw, x+(8*i), y, texcolor);

	}

}

uint32_t LoadFonts(uint8_t** bitfont){
	
	FILE* fp = NULL;
	bmh_t fhead;
	bmdib_t fdib;
	*bitfont = NULL;
	fp = fopen("DATA/FONT/font8x8.bmp","rb");
	
	if(fp==NULL){
		return 0 ;
	}
		
	fread(&fhead,sizeof(bmh_t),1,fp);
	fread(&fdib,sizeof(bmdib_t),1,fp);

	printf("File Size: %d bytes\n",fhead.fSize);
	printf("Res: %d x %d\n",fdib.iw,fdib.ih);
	printf("Planes: %d\n",fdib.planes);
	printf("Bits per pixel: %d\n",fdib.bpp);
	printf("Compression: %d\n",fdib.cmpr);
	printf("Image size: %d\n",fdib.iSize);
	printf("Nr pallete col: %d\n",fdib.ncolors);
	printf("Nr pallete imp col: %d\n",fdib.nimpcolors);
	printf("data offset: %d\n",fhead.dataOffset);

	
	fseek(fp,fhead.dataOffset,SEEK_SET);
	
	*bitfont = malloc(fdib.iSize*sizeof(uint8_t)); //1 bit per pixel: 8 pixels in a byte
	if(*bitfont == NULL)
		return 0;
	
	fread(*bitfont,sizeof(uint8_t),fdib.iSize,fp);
	
	fclose(fp);
	/*
	int i = 0, j = 0;
	fp = fopen("out.txt","w+");
	
	for(i=0;i<fdib.iSize;i++){
		if(i%16 == 0 && i>0)
			fputc('\n',fp);
		for(j=0;j<8;j++){
			if(((uint8_t)(*bitfont)[i]>>j) & 0x01)
				fputc('8',fp);
			else
				fputc('0',fp);
		}
		fputc(' ',fp);

	}
	
	fclose(fp);*/
	return fdib.iw;
}
