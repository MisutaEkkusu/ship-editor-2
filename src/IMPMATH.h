#ifndef _IMPMATH_H_
#define _IMPMATH_H_

#include <stdint.h>
#include <float.h>
#include <math.h>

#define sgn(x) x<0 ? -1 : 1

const float deg2rad;
const float rad2deg;

typedef struct vec2{
	
	float x;
	float y;
	
}vec2_t;

typedef struct vec3{
	
	float x;
	float y;
	float z;
	
}vec3_t;

typedef struct mat2{
	
	float m[4];
	
}mat2_t;

typedef struct mat3{
	
	float m[9];
	
}mat3_t;


uint32_t GetPowerOfTwo(uint32_t n);
float Lerp(float a, float b, float fact);



vec2_t V2_New(float x, float y);
vec2_t V2_Zero();
float  V2_Mag(vec2_t v);
float  V2_Dot(vec2_t v1, vec2_t v2);
vec2_t V2_Add(vec2_t v1, vec2_t v2);
vec2_t V2_Sub(vec2_t v1, vec2_t v2);
vec2_t V2_Negate(vec2_t v);
vec2_t V2_MatMul(vec2_t v, mat2_t m);
vec2_t V2_Lerp(vec2_t v1, vec2_t v2, float f);

vec3_t V3_New(float x, float y, float z);
vec3_t V3_Zero();
float  V3_Mag(vec3_t v);
float  V3_Dot(vec3_t v1, vec3_t v2);
vec3_t V3_Normalize(vec3_t v);
vec3_t V3_Cross(vec3_t v1, vec3_t v2);
vec3_t V3_NormCross(vec3_t v1, vec3_t v2);
vec3_t V3_Add(vec3_t v1, vec3_t v2);
vec3_t V3_Sub(vec3_t v1, vec3_t v2);
vec3_t V3_Negate(vec3_t v);
vec3_t V3_UniformScale(vec3_t v, float s);
vec3_t V3_NonUniformScale(vec3_t v, vec3_t s);
vec3_t V3_MatMul(vec3_t v, mat3_t m);


mat2_t M2_Ident();
mat2_t M2_Mul(mat2_t m1, mat2_t m2);
mat2_t M2_Euler(float ang_rad);


mat3_t M3_Ident();
mat3_t M3_Mul(mat3_t m1, mat3_t m2);
mat3_t M3_Euler(float ang_rad, char axis);
void   M3_Print(mat3_t m);

#endif
