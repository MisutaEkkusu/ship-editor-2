#include "BMPOPS.h"

uint8_t ReadBMP(char* filename, BMPFILE_t* BMP){
	
	FILE* fp = NULL;
	if((fp=fopen(filename,"rb")) == NULL){
		fprintf(stderr,"ERROR OPENING BMP!\n");
		return 0;
	}
	
	fread(&BMP->fHeader,sizeof(BMPFILEH_t),1,fp);
	fread(&BMP->iHeader,sizeof(BMPIMGH_t),1,fp);
	
	if(BMP->fHeader.BM != 0x4D42){
		fprintf(stderr,"ERROR: File is NOT Bitmap.\n");
		return 0;
	}
	printf("\nFILE HEADER:\n");
	printf("File size: %u\n",BMP->fHeader.fSize);
	printf("Pixed Data Offset: %u\n",BMP->fHeader.pixDataOffset);
	
	printf("\nIMAGE HEADER:\n");
	printf("Image resolution: %u x %u\n",BMP->iHeader.w,BMP->iHeader.h);
	printf("Bits per pixel: %u\n",BMP->iHeader.bpp);
	printf("Image size: %u\n",BMP->iHeader.imgSize);
	printf("Number of palette colors: %u\n",BMP->iHeader.palColorUsed);
	
	BMP->colData = malloc(BMP->iHeader.palColorUsed*sizeof(uint32_t));
	if(BMP->colData == NULL){
		fprintf(stderr,"ERROR ALLOCATING PALETTE MEMORY!\n");
		return 0;
	}
	fread(BMP->colData,sizeof(uint32_t),BMP->iHeader.palColorUsed,fp);
	
	BMP->pData = malloc(BMP->iHeader.imgSize*sizeof(uint8_t));
	if(BMP->pData == NULL){
		fprintf(stderr,"ERROR ALLOCATING PIXEL MEMORY!\n");
		return 0;
	}
	fread(BMP->pData,sizeof(uint8_t),BMP->iHeader.imgSize,fp);
	
	fclose(fp);
	return 1;
}

void DestroyBMP(BMPFILE_t* BMP){
	
	free(BMP->colData);
	free(BMP->pData);
	
}
