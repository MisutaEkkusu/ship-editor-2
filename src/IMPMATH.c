#include "IMPMATH.h"
#include <stdio.h>

const float deg2rad = M_PI/180.0f;
const float rad2deg = 180.0f/M_PI;

uint32_t GetPowerOfTwo(uint32_t n){
	
	uint32_t bitcount = 0;
	
	if(n&1 || n==0)
		return 0;
	
	while(n){
		
		n>>=1;
		bitcount++;
	}
	return bitcount-1;
} 

float Lerp(float a, float b, float fact){
	return a*(1.0f-fact)+b*fact;
}


//VEC2----------------------------

vec2_t V2_New(float x, float y){
	vec2_t ret = {x,y};
	return ret;
}

vec2_t V2_Zero(){
	vec2_t ret = {0.0f,0.0f};
	return ret;
} 

float V2_Mag(vec2_t v){
	return sqrt(v.x*v.x+v.y*v.y);
}

float V2_Dot(vec2_t v1, vec2_t v2){
	return v1.x*v2.x+v1.y*v2.y;
}

vec2_t V2_Add(vec2_t v1, vec2_t v2){
	v1.x+=v2.x;
	v1.y+=v2.y;
	return v1;
}

vec2_t V2_Sub(vec2_t v1, vec2_t v2){
	v1.x-=v2.x;
	v1.y-=v2.y;
	return v1;
}

vec2_t V2_Negate(vec2_t v){
	v.x = -v.x;
	v.y = -v.y;
	return v;
}

vec2_t V2_MatMul(vec2_t v, mat2_t m){
	
	uint8_t i, j, k;
	vec2_t ret;
	ret.x = 0.0f; ret.y = 0.0f;
	for(i=0;i<2;i++){
		for(j=0;j<2;j++){
			k = j + i*2;
			((float*)&ret)[i] += m.m[k]*((float*)&v)[j];
		}
	}
	return ret;
}

vec2_t V2_Lerp(vec2_t v1, vec2_t v2, float f){
	vec2_t r;
	r.x = v1.x*f + (1.0f-f)*v2.x;
	r.y = v1.y*f + (1.0f-f)*v2.y;
	return r;
}

//VEC3----------------------------

vec3_t V3_New(float x, float y, float z){
	vec3_t ret = {x, y, z};
	return ret;
}

vec3_t V3_Zero(){
	vec3_t ret = {0.0f, 0.0f, 0.0f};
	return ret;
}

float V3_Mag(vec3_t v){
	return sqrt(v.x*v.x+v.y*v.y+v.z*v.z);
}

float V3_Dot(vec3_t v1, vec3_t v2){
	return v1.x*v2.x+v1.y*v2.y+v1.z*v2.z;
}

vec3_t V3_Normalize(vec3_t v){
	
	float m = V3_Mag(v);
	if(m!=0.0f){
		m = 1.0f/m;
		v.x *= m;
		v.y *= m;
		v.z *= m;
	}

	return v;
}

vec3_t V3_Cross(vec3_t v1, vec3_t v2){
	vec3_t ret;
	ret.x = v1.y*v2.z - v1.z*v2.y;
	ret.y = v1.z*v2.x - v1.x*v2.z;
	ret.z = v1.x*v2.y - v1.y*v2.x;
	return ret;
}


vec3_t V3_NormCross(vec3_t v1, vec3_t v2){
	vec3_t ret = {0};	
	ret.x = v1.y*v2.z - v1.z*v2.y;
	ret.y = v1.z*v2.x - v1.x*v2.z;
	ret.z = v1.x*v2.y - v1.y*v2.x;
	
	ret = V3_Normalize(ret);
	
	return ret;
}

vec3_t V3_Add(vec3_t v1, vec3_t v2){
	v1.x+=v2.x;
	v1.y+=v2.y;
	v1.z+=v2.z;
	return v1;
}

vec3_t V3_Sub(vec3_t v1, vec3_t v2){
	v1.x-=v2.x;
	v1.y-=v2.y;
	v1.z-=v2.z;
	return v1;
}

vec3_t V3_Negate(vec3_t v){
	v.x = -v.x;
	v.y = -v.y;
	v.z = -v.z;
	return v;
}

vec3_t V3_UniformScale(vec3_t v, float s){
	v.x*=s;
	v.y*=s;
	v.z*=s;
	return v;
}

vec3_t V3_NonUniformScale(vec3_t v, vec3_t s){
	v.x*=s.x;
	v.y*=s.y;
	v.z*=s.z;
	return v;
}

vec3_t V3_MatMul(vec3_t v, mat3_t m){
	
	uint8_t i, j, k;
	vec3_t ret;
	ret.x = 0.0f; ret.y = 0.0f; ret.z = 0.0f;
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			k = j + i*3;
			((float*)&ret)[i] += m.m[k]*((float*)&v)[j];
		}
	}
	return ret;
}

//MATRIX2----------------------------

mat2_t M2_Ident(){
	mat2_t ret;
	ret.m[0] = 1.0f; ret.m[1] = 0.0f;
	ret.m[2] = 0.0f; ret.m[3] = 1.0f;
	return ret;
}

mat2_t M2_Mul(mat2_t m1, mat2_t m2){
	
	uint8_t i, j, k1, k2, k3, n;
	mat2_t ret;
	
	for(j=0;j<2;j++){
		for(i=0;i<2;i++){
			k3 = i+j*2;
			for(n=0;n<2;n++){
				k1 = i+n*2;
				k2 = n+j*2;
				ret.m[k3] += m1.m[k1]*m2.m[k2];
			}
		}
	}
	
}

mat2_t M2_Euler(float ang_rad){
	
	mat2_t m;
	
	m.m[0] = cos(ang_rad); m.m[1] = -sin(ang_rad);
	m.m[2] = sin(ang_rad); m.m[2] =  cos(ang_rad);
	
	return m;
}

//MATRIX3----------------------------
mat3_t M3_Ident(){
	mat3_t ret;
	ret.m[0] = 1.0f; ret.m[1] = 0.0f; ret.m[2] = 0.0f;
	ret.m[3] = 0.0f; ret.m[4] = 1.0f; ret.m[5] = 0.0f;
	ret.m[6] = 0.0f; ret.m[7] = 0.0f; ret.m[8] = 1.0f;
	return ret;
}

mat3_t M3_Mul(mat3_t m1, mat3_t m2){
	
	uint8_t i, j, k1, k2, k3, n;
	mat3_t ret;
	
	for(j=0;j<3;j++){
		for(i=0;i<3;i++){
			k3 = i+j*3;
			for(n=0;n<3;n++){
				k1 = i+n*3;
				k2 = n+j*3;
				ret.m[k3] += m1.m[k1]*m2.m[k2];
			}
		}
	}
	
}

mat3_t M3_Euler(float ang_rad, char axis){
	
	mat3_t ret;
	
	switch(axis){
		
		case 'x':
			ret.m[0] = 1.0f; 	ret.m[1] = 0.0f; 			ret.m[2] = 0.0f;
			ret.m[3] = 0.0f; 	ret.m[4] = cos(ang_rad); 	ret.m[5] = -sin(ang_rad);
			ret.m[6] = 0.0f; 	ret.m[7] = sin(ang_rad); 	ret.m[8] = cos(ang_rad);
			break;
		
		case 'y':
			ret.m[0] = cos(ang_rad); 	ret.m[1] = 0.0f; 	ret.m[2] = -sin(ang_rad);
			ret.m[3] = 0.0f; 			ret.m[4] = 1.0f; 	ret.m[5] = 0.0f;
			ret.m[6] = sin(ang_rad); 	ret.m[7] = 0.0f; 	ret.m[8] = cos(ang_rad);		
			break;
		
		case 'z':
			ret.m[0] = cos(ang_rad); 	ret.m[1] = -sin(ang_rad); 	ret.m[2] = 0.0f;
			ret.m[3] = sin(ang_rad); 	ret.m[4] = cos(ang_rad); 	ret.m[5] = 0.0f;
			ret.m[6] = 0.0f; 			ret.m[7] = 0.0f; 			ret.m[8] = 1.0f;		
			break;

		default:
			ret = M3_Ident();
			break;
	}
	
	return ret;
}

void M3_Print(mat3_t m){
	uint8_t i = 0;
		
	for(i=0;i<9;i++){
		
		if(i>0 && i%3==0)
			putc('\n',stdout);
		
		printf("%f ",m.m[i]);	
	}
	putc('\n',stdout);
	
}
