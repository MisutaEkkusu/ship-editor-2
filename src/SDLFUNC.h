#ifndef _SDLFUNC_H_
#define _SDLFUNC_H_


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <SDL2/SDL.h>

struct mouse{
	
	int32_t x;
	int32_t y;
	float xf;
	float yf;
	uint32_t state;
	int32_t wheel;
	
} cursor;

typedef uint8_t bool;
bool mLquit; //main loop quit boolean

uint32_t *fbuffer, *dbuffer;
uint32_t screenX, screenY;

SDL_Event event;
SDL_Window * window;
SDL_Renderer * renderer;
SDL_Texture * texture;

bool InitGraphics(uint32_t flags, uint32_t w, uint32_t h);
void UpdateFB(uint32_t* src, uint32_t pitch);
bool CloseGraphics(void);
void ParseInput(void);

#endif
