#include "SHIPDEF.h"

//data array equivalent of a fread
void dread(void* output, const void* data, size_t sz, size_t* offset){
	memcpy(output, (data+*offset), sz);
	*offset += sz;
}

/*
uint8_t ShpReadMesh(uint8_t* data, mesh_t* msh){
	
	size_t dataoffs = 0;
	
	dread(&msh->meshCount,data,sizeof(int32_t),&dataoffs);
	dread(&msh->vertLen,data,sizeof(int32_t),&dataoffs);
	dread(&msh->normLen,data,sizeof(int32_t),&dataoffs);
	dread(&msh->uvsLen ,data,sizeof(int32_t),&dataoffs);
	dread(&msh->triLen,data,sizeof(int32_t),&dataoffs);
	
	printf("Mesh Count:   %d\n",msh->meshCount);
	printf("Vert Count:   %d\n",msh->vertLen);
	printf("Normal Count: %d\n",msh->normLen);
	printf("Uv Count:     %d\n",msh->uvsLen);
	printf("Tri Count:    %d\n",msh->triLen/3);
	
	msh->verts = malloc(msh->vertLen*sizeof(vec3_t));
	msh->norms = malloc(msh->normLen*sizeof(vec3_t));		
	msh->uvs = malloc(msh->uvsLen*sizeof(vec2_t));
	msh->triidx = malloc(msh->triLen * sizeof(int32_t));
	
	if(msh->verts == NULL || msh->norms == NULL || msh->uvs == NULL || msh->triidx == NULL){
		printf("Failed to allocate mesh memory!\n");
		return 0;
	}
	
	dread(msh->verts,data,msh->vertLen*sizeof(vec3_t),&dataoffs);
	dread(msh->norms,data,msh->normLen*sizeof(vec3_t),&dataoffs);
	dread(msh->uvs,data,msh->uvsLen*sizeof(vec2_t),&dataoffs);
	dread(msh->triidx,data,msh->triLen*sizeof(int32_t),&dataoffs);
	
	return 1;
}
*/

uint8_t ShpReadFileMesh(FILE* fp, mesh_t* msh){
	
	fread(&msh->meshCount,sizeof(int32_t),1,fp);
	fread(&msh->vertLen  ,sizeof(int32_t),1,fp);
	fread(&msh->normLen  ,sizeof(int32_t),1,fp);
	fread(&msh->uvsLen   ,sizeof(int32_t),1,fp);
	fread(&msh->triLen   ,sizeof(int32_t),1,fp);
	
	printf("Mesh Count:   %d\n",msh->meshCount);
	printf("Vert Count:   %d\n",msh->vertLen);
	printf("Normal Count: %d\n",msh->normLen);
	printf("Uv Count:     %d\n",msh->uvsLen);
	printf("Tri Count:    %d\n",msh->triLen/3);
	
	msh->verts = malloc(msh->vertLen*sizeof(vec3_t));
	msh->norms = malloc(msh->normLen*sizeof(vec3_t));		
	msh->uvs = malloc(msh->uvsLen*sizeof(vec2_t));
	msh->triidx = malloc(msh->triLen * sizeof(int32_t));
	
	if(msh->verts == NULL || msh->norms == NULL || msh->uvs == NULL || msh->triidx == NULL){
		printf("Failed to allocate mesh memory!\n");
		return 0;
	}
	
	fread(msh->verts,sizeof(vec3_t),msh->vertLen,fp);
	fread(msh->norms,sizeof(vec3_t),msh->normLen,fp);
	fread(msh->uvs,sizeof(vec2_t),msh->uvsLen,fp);
	fread(msh->triidx,sizeof(int32_t),msh->triLen,fp);
	
	/*
	char filename[MAXSTRLEN];
	memset(filename,0x00,MAXSTRLEN);
	snprintf(filename,MAXSTRLEN,"mdl%d.obj",(rand()%255)+1);
	ExportMeshOBJ(*msh, filename);
		
	free(msh->verts);
	free(msh->norms);
	free(msh->uvs);
	free(msh->triidx);
	*/
	
	return 1;
}


uint8_t ReadFileString(FILE* fp, char* outstr){
	uint8_t strLen = 0;
	fread(&strLen,sizeof(uint8_t),1,fp);
	memset(outstr,0x00,MAXSTRLEN);
	fread(outstr,sizeof(uint8_t),strLen,fp);
	return strLen;
}

uint8_t ReadDataString(uint8_t* data, char* outstr){
	uint8_t strLen = 0;
	strLen = data[0];
	memset(outstr,0x00,MAXSTRLEN);	
	strncpy(outstr,(char*)(data+1),strLen);
	return strLen;
}

container_type_t GetContainerType(char* cname){
	
	if(strncmp(cname,"WorkshopID",10)==0)
		return WSID;
	else if(strncmp(cname,"WorkshopTitle",13)==0)
		return WSTITLE;
	else if(strncmp(cname,"WorkshopDescription",19)==0)
		return WSDESCR;
	else if(strncmp(cname,"WorkshopVisibility",18)==0)
		return WSVISIB;
	else if(strncmp(cname,"WorkshopTags",12)==0)
		return WSTAGS;	
	else if(strncmp(cname,"WorkshopPreview",15)==0)
		return WSPREV;
	else if(strncmp(cname,"Pos",3)==0)
		return SHPPOS;	
	else if(strncmp(cname,"Rot",3)==0)
		return SHPROT;
	else if(strncmp(cname,"Scale",5)==0)
		return SHPSCL;	
	else if(strncmp(cname,"ShipMesh",8)==0)
		return SHPMSH;	
	else if(strncmp(cname,"CockpitMesh",11)==0)
		return CPTMSH;	
	else if(strncmp(cname,"ShipDiffuse",11)==0)
		return SHPTXD;	
	else if(strncmp(cname,"CockpitDiffuse",14)==0)
		return CPTTXD;	
	else if(strncmp(cname,"ShipIllum",9)==0)
		return SHPTXI;	
	else if(strncmp(cname,"CockpitIllum",13)==0)
		return CPTTXI;
	else if(strncmp(cname,"Transparency",13)==0)
		return TRPRCY;		
	else if(strncmp(cname,"Engines",7)==0)
		return ENGINS;
	else if(strncmp(cname,"EngineToggles",13)==0)
		return ENGTOG;
	else if(strncmp(cname,"TrailSizes",10)==0)
		return TRAISZ;
	else if(strncmp(cname,"Contrails",9)==0)
		return CONTLS;
	else if(strncmp(cname,"EngineDim",9)==0)
		return ENGDIM;
	else if(strncmp(cname,"EngineBright",12)==0)
		return ENGBRT;
	else if(strncmp(cname,"HandlingSettings",15)==0)
		return HNDSET;
	else
		return UNKNWN;
}

void ExportMeshOBJ(mesh_t msh, const char* filename){
	
	FILE* fp = fopen(filename,"w+");
	int32_t i = 0;
	
	for(i=0;i<msh.vertLen;i++)
		fprintf(fp,"v %f %f %f\n",msh.verts[i].x,msh.verts[i].y,msh.verts[i].z);
	for(i=0;i<msh.normLen;i++)
		fprintf(fp,"vn %f %f %f\n",msh.norms[i].x,msh.norms[i].y,msh.norms[i].z);
	for(i=0;i<msh.uvsLen;i++)
		fprintf(fp,"vt %f %f\n",msh.uvs[i].x,msh.uvs[i].y);
	
	fprintf(fp,"g %s\n",filename);	
	
	for(i=0;i<msh.triLen;i+=3){
		fprintf(fp,"f %d/%d/%d %d/%d/%d %d/%d/%d\n",msh.triidx[i]+1,msh.triidx[i]+1,msh.triidx[i]+1,
													msh.triidx[i+1]+1,msh.triidx[i+1]+1,msh.triidx[i+1]+1,
													msh.triidx[i+2]+1,msh.triidx[i+2]+1,msh.triidx[i+2]+1);
	}
	fclose(fp);
	
}

ERRTYPE_t LoadSHPFile(const char* filename, SHP_file_t* shpF){
	
	FILE* fp = NULL;
	uint32_t i = 0;
	uint64_t curpos;
	
	fp=fopen(filename,"rb");
	
	if(fp == NULL){
		puts("ERROR: File not found\n");
		return INOUT;
	}
	
	ReadFileString(fp,shpF->type);
	printf("File Type: %s\n\n",shpF->type);
	
	if(strncmp(shpF->type,"Ship2",5)!=0){
		printf("File is not a v2 ship file format!\n");
		return NOTV2; 
	}
	fread(&shpF->ncontainer,sizeof(int32_t),1,fp);
	printf("Container count: %u\n",shpF->ncontainer);
	
	shpF->container = calloc(shpF->ncontainer,sizeof(SHP_container_t));
	if(shpF->container == NULL){
		printf("Failed to allocate container memory!\n");
		return ALLOC;
	}
	for(i=0;i<shpF->ncontainer;i++){
		
		ReadFileString(fp,shpF->container[i].name);
		
		shpF->container[i].type = GetContainerType(shpF->container[i].name);
		
		fread(&shpF->container[i].startAddress,sizeof(int64_t),1,fp);
		printf("%s / 0x%016lX", shpF->container[i].name,shpF->container[i].startAddress);
		
		curpos = ftell(fp);
		
		fseek(fp,shpF->container[i].startAddress,SEEK_SET);
		
		fread(&shpF->container[i].size,sizeof(int32_t),1,fp);

		/*
		shpF->container[i].data = calloc(shpF->container[i].size,sizeof(uint8_t));
		if(shpF->container[i].data == NULL){
			printf("Failed to allocate container data.\n");
			return ALLOC;
		}
		else{
			fread(shpF->container[i].data,sizeof(uint8_t),shpF->container[i].size,fp);
		}
		*/		
		printf("/ BlockSize: %d\n",shpF->container[i].size);
		switch(shpF->container[i].type){
			
			case WSTITLE:
				ReadFileString(fp,shpF->wsData.wsTitle);
				printf("Title: %s\n",shpF->wsData.wsTitle);
				break;
			case WSDESCR:
				ReadFileString(fp,shpF->wsData.wsDescription);
				printf("Description: %s\n",shpF->wsData.wsDescription);
				break;
			case SHPMSH:
				if(ShpReadFileMesh(fp, &shpF->shp_msh)==0){
					puts("Failed to load ShipMesh");
					return FATAL;
				}
				break;
			case CPTMSH:
				if(ShpReadFileMesh(fp, &shpF->cpt_msh)==0){
					puts("Failed to load CockpitMesh");
					return FATAL;
				}
				break;
			default:
			
				break;
		}
		
		fseek(fp,curpos,SEEK_SET);
		
		
	}
	
	printf("\n");
	
	fclose(fp);
	
	
	return NOERR;
}

ERRTYPE_t CleanSHPFile(SHP_file_t* shpF){
	
	uint32_t i = 0;
	
	for(i=0;i<shpF->ncontainer;i++)
		free(shpF->container[i].data);
		
	free(shpF->container);
	free(shpF->shp_msh.verts);
	free(shpF->shp_msh.norms);
	free(shpF->shp_msh.uvs);
	free(shpF->shp_msh.triidx);
	free(shpF->cpt_msh.verts);
	free(shpF->cpt_msh.norms);
	free(shpF->cpt_msh.uvs);
	free(shpF->cpt_msh.triidx);
	
	return NOERR;
	
}

/*
container_type_t GetContainerTypeV1(char* cname){
	
	if(strncmp(cname,"workshopid",10)==0)
		return WSID;
	else if(strncmp(cname,"workshoptitle",13)==0)
		return WSTITLE;
	else if(strncmp(cname,"workshopdescription",19)==0)
		return WSDESCR;
	else if(strncmp(cname,"workshopvisibility",18)==0)
		return WSVISIB;
	else if(strncmp(cname,"workshoptags",12)==0)
		return WSTAGS;
	else if(strncmp(cname,"shipverts",9)==0)
		return SHPVTX;
	else if(strncmp(cname,"shipnormals",11)==0)
		return SHPNRM;
	else if(strncmp(cname,"shipuvs",7)==0)
		return SHPUVS;
	else if(strncmp(cname,"shiptris",8)==0)
		return SHPTRI;
	else if(strncmp(cname,"shiptexture",11)==0)
		return SHPTEX;
	else if(strncmp(cname,"enginepositions",15)==0)
		return ENGPOS;
	else if(strncmp(cname,"leftcontrail",12)==0)
		return LCONTR;
	else if(strncmp(cname,"rightcontrail",13)==0)
		return RCONTR;
	else if(strncmp(cname,"engineCol",9)==0)
		return ENGCL1;
	else if(strncmp(cname,"enginecol2",10)==0)
		return ENGCL2;
	else if(strncmp(cname,"hudcolor",8)==0)
		return HUDCOL;
	else if(strncmp(cname,"settings",8)==0)
		return SETTNG;
	else
		return UNKNWN;
}
*/
