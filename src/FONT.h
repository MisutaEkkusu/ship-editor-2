#ifndef _FONT_H_
#define _FONT_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define TEXCOLOR 0x0000FF00

typedef struct bmh{
	
	uint16_t sig;
	uint32_t fSize;
	uint32_t reserved;
	uint32_t dataOffset;
	
}__attribute__((packed)) bmh_t;

typedef struct bmdib{
	
	uint32_t hSize;
	uint32_t iw;
	uint32_t ih;
	uint16_t planes;
	uint16_t bpp;
	uint32_t cmpr; //compression
	uint32_t iSize; //image data size
	uint32_t xppm; //resolution
	uint32_t yppm; //pixels per meter
	uint32_t ncolors; //colors in pallete
	uint32_t nimpcolors; //important colors in pallete
	//uint32_t bitmask[4]; //RGBA bitmask
	
} bmdib_t;

uint8_t* bitfont;
uint32_t bitfont_width;

void BlitChar(char c, uint32_t* fbuffer, uint32_t fw, uint32_t x, uint32_t y, uint32_t texcolor);
void BlitText(uint32_t* fbuffer, uint32_t fw, char* text, uint32_t x, uint32_t y, uint32_t texcolor);
uint32_t LoadFonts(uint8_t** bitfont);


#endif
