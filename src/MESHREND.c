#include "MESHREND.h"


void RegBitSet(uint8_t *reg, uint8_t offs){
	*reg |= (0x01<<offs);
}

void RegBitUnSet(uint8_t *reg, uint8_t offs){
	*reg &= ~(0x01<<offs);
}

uint8_t RegBitCheck(uint8_t reg, uint8_t offs){
	return (reg & (0x01<<offs));

}

uint8_t MSH_SetViewMesh(mesh_t in){
	
	uint32_t i = 0, j = 0;
	
	viewMesh.meshCount = in.meshCount;
	viewMesh.vertLen = in.vertLen;
	viewMesh.normLen = in.normLen;
	viewMesh.uvsLen = in.uvsLen;
	viewMesh.triLen = in.triLen;
	
	viewMesh.verts = malloc(in.vertLen*sizeof(vec3_t));
	viewMesh.norms = malloc(in.normLen*sizeof(vec3_t));
	viewMesh.uvs = malloc(in.uvsLen*sizeof(vec2_t));
	viewMesh.triidx = malloc(in.triLen*sizeof(int32_t));
	
		
	if(viewMesh.verts == NULL || viewMesh.norms == NULL || viewMesh.uvs == NULL || viewMesh.triidx == NULL){
		printf("Failed to allocate mesh memory!\n");
		return 0;
	}
	
	memcpy(viewMesh.verts,in.verts,in.vertLen*sizeof(vec3_t));
	memcpy(viewMesh.norms,in.norms,in.normLen*sizeof(vec3_t));
	memcpy(viewMesh.uvs,in.uvs,in.uvsLen*sizeof(vec2_t));
	memcpy(viewMesh.triidx,in.triidx,in.triLen*sizeof(int32_t));
	
	
	viewMeshTris = calloc(in.triLen/3,sizeof(tri_t));
	
	if(viewMeshTris == NULL){
		printf("Failed to allocate triangle list memory!\n");
		return 0;
	}
	
	for(i=0;i<in.triLen/3;i++){
		j = 3*i;
		viewMeshTris[i].vi[0] = viewMesh.triidx[j];
		viewMeshTris[i].vi[1] = viewMesh.triidx[j+1];
		viewMeshTris[i].vi[2] = viewMesh.triidx[j+2];
	}
	
	return 1;
}

void MSH_FreeViewMesh(){
	
	free(viewMesh.verts);
	free(viewMesh.norms);
	free(viewMesh.uvs);
	free(viewMesh.triidx);
	free(viewMeshTris);
	
}

void MSH_IdentVerts(mesh_t in){
	
	memcpy(viewMesh.verts,in.verts,viewMesh.vertLen*sizeof(vec3_t));		
	//memcpy(viewMesh.norms,in.norms,viewMesh.normLen*sizeof(vec3_t));		
	
}

void MSH_Translate(mesh_t* msh, vec3_t trs){
	uint32_t i = 0;
	for(i=0;i<msh->vertLen;i++){
		
		msh->verts[i] = V3_Add(msh->verts[i],trs);
		
	}

}
void MSH_Rotate(mesh_t* msh, vec3_t rot){		//Euler X->Y->Z rotation or more

	uint32_t i = 0;
	RX = M3_Euler(rot.x,'x');
	RY = M3_Euler(rot.y,'y');
	RZ = M3_Euler(rot.z,'z');
	
	for(i=0;i<msh->vertLen;i++){
		
		msh->verts[i] = V3_MatMul(msh->verts[i],RZ);
		msh->verts[i] = V3_MatMul(msh->verts[i],RX);
		msh->verts[i] = V3_MatMul(msh->verts[i],RY);
		//msh->norms[i] = V3_MatMul(msh->norms[i],RX);
		//msh->norms[i] = V3_MatMul(msh->norms[i],RY);
		//msh->norms[i] = V3_MatMul(msh->norms[i],RZ);		
	}

}


void MSH_Scale(mesh_t* msh, vec3_t scl){
	uint32_t i = 0;
	for(i=0;i<msh->vertLen;i++){
		
		msh->verts[i] = V3_NonUniformScale(msh->verts[i],scl);
		
	}
}

void MSH_CalcTriZMid(){
	uint32_t i = 0, j = 0;
	for(i=0;i<viewMesh.triLen;i+=3){
		
		viewMeshTris[j].zmid = 0x0000FFFF*(camera.pos.z+(viewMesh.verts[viewMeshTris[j].vi[0]].z+
													  viewMesh.verts[viewMeshTris[j].vi[1]].z+
													  viewMesh.verts[viewMeshTris[j].vi[2]].z)/3.0f)+1;
		
		viewMeshTris[j].zi = (uint32_t)round(viewMeshTris[j].zmid);
		
		j++;
	}
	
}

//RENDER FUNCTIONS


void REND_SetupRenderEngine(){
	
	uint16_t i, j;
	uint8_t c = 0x00; 
	
	RenderMode = 0x00;
	RegBitSet(&RenderMode,BACKFACE_CULL);
	RegBitSet(&RenderMode,M_TEXTURED);
	/*
	viewMeshTexture.pixdata = calloc(256*256,sizeof(uint32_t));
	if(viewMeshTexture.pixdata==NULL)
		return;
	
	viewMeshTexture.xi = 256;
	viewMeshTexture.yi = 256;
	
	for(j=0;j<viewMeshTexture.xi;j++){
		for(i=0;i<viewMeshTexture.yi;i++){
			c = (i)^(j);
			viewMeshTexture.pixdata[i+(j*viewMeshTexture.xi)] = 0x00000000 | (c<<16) | (c<<8) | c;
		}
	}
	*/
}


void REND_PutPixel(uint16_t x, uint16_t y, uint32_t col){
	
	fbuffer[x+((y<<8) + (y<<6))] = col;
	
}

void REND_DrawScanLine(uint16_t x1, uint16_t x2, uint16_t y, uint32_t col){
	

	while(x1<=x2){
		
		*(fbuffer+((y<<8)+(y<<6))+x1) = col;
		
		x1++;
	}
	
}

void REND_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint32_t col)
{
	int16_t i=0,j=0,dx=0,dy=0,sdx=0,sdy=0,dxabs=0,dyabs=0,x=0,y=0,px=0,py=0;
	
	if(x1>x2 || y1>y2){
		
		uint16_t temp=x2;
		x2=x1;
		x1=temp;
		temp=y2;
		y2=y1;
		y1=temp;
		
	}
	
	dx=x2-x1;
	dy=y2-y1;
	
	if(dy==0){
		/*
		for(i=x1;i<x2;i++){
			REND_PutPixel(i,y1,col);
		}
		*/
		REND_DrawScanLine(x1,x2,y1,col);
		return;
	}
	
	else if(dx==0){
		
		for(i=y1;i<=y2;i++){
			REND_PutPixel(x1,i,col);
		}
		return;
	}

	dxabs=abs(dx);
	dyabs=abs(dy);
	sdx=sgn(dx);
	sdy=sgn(dy);
	x=dyabs>>1;
	y=dxabs>>1;
	px=x1;
	py=y1;
	
	REND_PutPixel(px,py,col);

	if (dxabs>=dyabs)
	{
		for(i=0;i<dxabs;i++)
		{
			y+=dyabs;
			if (y>=dxabs)
			{
				y-=dxabs;
				py+=sdy;
			}
			px+=sdx;

			REND_PutPixel(px,py,col);
			
		}
	}
	else
	{
		for(i=0;i<dyabs;i++)
		{
			x+=dxabs;
				if (x>=dyabs)
				{
					x-=dyabs;
					px+=sdx;
				}
				py+=sdy;
			
			REND_PutPixel(px,py,col);
		}
	}
 
}

void REND_SwapTriVTX(vec3_t* v1, vec3_t* v2){
	
	vec3_t temp = *v1;
	
	*v1 = *v2;
	*v2 = temp;
	
}

void REND_SortTriVTX(vec3_t* vptr){
	
	uint8_t i=0, j=0;
	
	for(i=0;i<2;i++)
		for(j=0;j<2-i;j++)
			if(vptr[j].y>vptr[j+1].y)
				REND_SwapTriVTX(&vptr[j],&vptr[j+1]);
	
}

uint8_t REND_IsVInScreen(vec3_t v){
	uint16_t x, y;
	x = (uint16_t) round(v.x+(WRENX>>1));
	y = (uint16_t) round(v.y+(WRENY>>1));
	if(x>WRENX || y>WRENY)
		return 0;
	else
		return 1;
	
}


void REND_DrawFlatColTriangle_FBottom(int16_t x0, int16_t y0,
									  int16_t x1, int16_t y1,
									  int16_t x2, int16_t y2,
									  uint32_t col)
{

	float s1, s2, invslp1, invslp2;
	int16_t i = 0;
	
	if(x2<x1){
		
		int16_t temp = x1;
		x1 = x2;
		x2=temp;
		temp = y1;
		y1 = y2;
		y2 = temp;
	}	
	
	s1 = x0; s2 = x0;
	invslp1 = (1.0f*(x1-x0))/(y1-y0);
	invslp2 = (1.0f*(x2-x0))/(y2-y0);
	
	for(i=y0;i<=y2;i++){
		
		
		REND_DrawScanLine(s1, s2, i, col);

		s1 += invslp1;
		s2 += invslp2;

	}

}

void REND_DrawFlatColTriangle_FTop(int16_t x0, int16_t y0,
								   int16_t x1, int16_t y1,
								   int16_t x2, int16_t y2,
								   uint32_t col)
{

		
	float s1, s2, invslp1, invslp2;
	int16_t i = 0;
	
	if(x1<x0){
		int16_t temp = x1;
		x1 = x0;
		x0=temp;
		temp = y1;
		y1 = y0;
		y0 = temp;
	}	
	
	s1 = x0; s2 = x1;
	invslp1 = (1.0f*(x0-x2))/(y0-y2);
	invslp2 = (1.0f*(x1-x2))/(y1-y2);
	
	for(i=y0;i<=y2;i++){
		
		
		REND_DrawScanLine(s1, s2, i, col);

		s1 += invslp1;
		s2 += invslp2;

	}

}


void REND_DrawFlatColTriangle(vec3_t v1, vec3_t v2, vec3_t v3, uint32_t col){
	
	uint8_t i = 0, j = 0;
	
	vec3_t verts[4];
	
	i = REND_IsVInScreen(v1) & REND_IsVInScreen(v2) & REND_IsVInScreen(v3);
	
	if(i==0) //one of the triangle's vertices isn't on screen
		return; //ignore triangle (cheap solution lol)
	
	verts[0] = v1;
	verts[1] = v2;
	verts[2] = v3;

	
	for(i=0;i<3;i++){
		verts[i].x = (int16_t)round(verts[i].x + (WRENX>>1)); 
		verts[i].y = (int16_t)round(verts[i].y + (WRENY>>1));
	}
	REND_SortTriVTX(verts);

	
	if(verts[0].y == verts[1].y){
		
		//DRAW FLAT TOP TRIANGLE
		REND_DrawFlatColTriangle_FTop((int16_t)verts[0].x,(int16_t)verts[0].y,
									  (int16_t)verts[1].x,(int16_t)verts[1].y,
									  (int16_t)verts[2].x,(int16_t)verts[2].y,
									  col);
		
	}
	
	else if(verts[1].y == verts[2].y){
		
		//DRAW FLAT BOTTOM TRIANGLE
		REND_DrawFlatColTriangle_FBottom((int16_t)verts[0].x,(int16_t)verts[0].y,
									     (int16_t)verts[1].x,(int16_t)verts[1].y,
									     (int16_t)verts[2].x,(int16_t)verts[2].y,
									     col);
		
	}
	
	else{
		
		verts[3].y = verts[1].y; //calculate split
		 
		verts[3].x = ((verts[3].y-verts[0].y)*(verts[2].x-verts[0].x))/(verts[2].y-verts[0].y)+verts[0].x;
		
		REND_DrawFlatColTriangle_FTop((int16_t)verts[3].x, (int16_t)verts[3].y,
									  (int16_t)verts[1].x, (int16_t)verts[1].y,
									  (int16_t)verts[2].x, (int16_t)verts[2].y,
									  col);
									  
		REND_DrawFlatColTriangle_FBottom((int16_t)verts[0].x,(int16_t)verts[0].y,
										 (int16_t)verts[1].x,(int16_t)verts[1].y,
										 (int16_t)verts[3].x,(int16_t)verts[3].y,
										 col);
	}
	
}

void REND_DrawWireColTriangle(vec3_t v1, vec3_t v2, vec3_t v3, uint32_t col){
	
	uint16_t sx1, sy1, sx2, sy2, sx3, sy3;
	
	sx1 = (uint16_t)round(v1.x+(WRENX>>1));		sy1 = (uint16_t)round((v1.y+(WRENY>>1)));
	sx2 = (uint16_t)round(v2.x+(WRENX>>1)); 	sy2 = (uint16_t)round((v2.y+(WRENY>>1)));
	sx3 = (uint16_t)round(v3.x+(WRENX>>1)); 	sy3 = (uint16_t)round((v3.y+(WRENY>>1)));
					
	
	if(sx1 > WRENX || sy1 > WRENY || 
	   sx2 > WRENX || sy2 > WRENY ||
	   sx3 > WRENX || sy3 > WRENY){
			return;   
	}
	
	REND_DrawLine(sx1,sy1,sx2,sy2,col);
	REND_DrawLine(sx2,sy2,sx3,sy3,col);
	REND_DrawLine(sx3,sy3,sx1,sy1,col);
	
}


/* This function takes last element as pivot, places 
   the pivot element at its correct position in sorted 
    array, and places all smaller (smaller than pivot) 
   to left of pivot and all greater elements to right 
   of pivot */

void swapTri(tri_t* t1, tri_t* t2) 
{ 
    tri_t t = *t1; 
    *t1 = *t2; 
    *t2 = t; 
}    
   
int partition (tri_t* tris, int low, int high) 
{ 
    tri_t pivot = tris[high];    // pivot 
	int t;
    int i = (low - 1);  // Index of smaller element 
  
    for (int j = low; j <= high - 1; j++) 
    { 
        // If current element is smaller than or 
        // equal to pivot 
        if (tris[j].zi <= pivot.zi) 
        { 
            i++;    // increment index of smaller element 
            swapTri(&tris[i], &tris[j]); 
        } 
    } 
    swapTri(&tris[i + 1], &tris[high]); 
    return (i + 1); 
} 
  
/* The main function that implements QuickSort 
 arr[] --> Array to be sorted, 
  low  --> Starting index, 
  high  --> Ending index */
void quickSort(tri_t* tris, int low, int high) 
{ 
    if (low < high) 
    { 
        /* pi is partitioning index, arr[p] is now 
           at right place */
        int pi = partition(tris, low, high); 
  
        // Separately sort elements before 
        // partition and after partition 
        quickSort(tris, low, pi - 1); 
        quickSort(tris, pi + 1, high); 
    } 
}


void REND_ZSortTris(){
	tri_t temp;
	uint32_t i = 0, j = 0;
	uint32_t trisize = viewMesh.triLen/3;
	
	quickSort(viewMeshTris,0,trisize-1);
	//quickSort(viewMeshTris,trisize/2,trisize);
	/*
	for(i=0;i<trisize-1;i++)
		for(j=0;j<trisize-1-i;j++)
			if(viewMeshTris[j].zi>viewMeshTris[j+1].zi){
				temp = viewMeshTris[j];
				viewMeshTris[j] = viewMeshTris[j+1];
				viewMeshTris[j+1] = temp;
			}
	*/
}

void REND_DrawMesh(){
	vec3_t norm, vd1,vd2;
	uint32_t col, i = 0;
	
	for(i=0;i<viewMesh.triLen/3;i++){
		
		
		if(RegBitCheck(RenderMode, M_WIREFRAME)){
			col = 0x0000FF00;
			REND_DrawWireColTriangle(viewMesh.verts[viewMeshTris[i].vi[0]],
									 viewMesh.verts[viewMeshTris[i].vi[1]],
									 viewMesh.verts[viewMeshTris[i].vi[2]],
									 col);
			continue;
		}
	
		vd1 = V3_Sub(viewMesh.verts[viewMeshTris[i].vi[1]],viewMesh.verts[viewMeshTris[i].vi[0]]);
		vd2 = V3_Sub(viewMesh.verts[viewMeshTris[i].vi[2]],viewMesh.verts[viewMeshTris[i].vi[0]]);
	
		norm = V3_NormCross(vd1,vd2);		
		
		if(RegBitCheck(RenderMode, BACKFACE_CULL)){

			if(V3_Dot(norm,camera.dir)>0.0f)
				continue;

		}
		
		if(RegBitCheck(RenderMode, M_SHADED))
			col = ((uint32_t)round(0x000000FF * (0.5+0.5*fabs(V3_Dot(norm,camera.dir)))))<<8;
		else
			col = (0x000000FF & (viewMeshTris[i].zi>>16))<<8;
		

		if(RegBitCheck(RenderMode, M_TEXTURED)){	
			
			col = ((uint32_t)round(0x000000FF * (0.5+0.5*fabs(V3_Dot(norm,camera.dir)))));
			REND_DrawTextureTriangle(viewMesh.verts[viewMeshTris[i].vi[0]], viewMesh.verts[viewMeshTris[i].vi[1]], viewMesh.verts[viewMeshTris[i].vi[2]],
									 viewMesh.uvs[viewMeshTris[i].vi[0]], viewMesh.uvs[viewMeshTris[i].vi[1]], viewMesh.uvs[viewMeshTris[i].vi[2]],
									 viewMeshTexture, col);
		}
		else{
			REND_DrawFlatColTriangle(viewMesh.verts[viewMeshTris[i].vi[0]],
									 viewMesh.verts[viewMeshTris[i].vi[1]],
									 viewMesh.verts[viewMeshTris[i].vi[2]],
									 col);
		}

		
	}
	
}

//TEXTURED TRIANGLE

void REND_DrawTextureScanline(uint16_t x0, uint16_t x1, uint16_t y, vec2_t uv0, vec2_t uv1, texture_t tex, uint32_t col){
	
	uint16_t i = 0;
	float uvinterp = 0.0f, dcx;
	uint32_t u, v;
	uint32_t tcol;
	
	if(x1<x0){
		uint16_t temp = x0;
		x0 = x1;
		x1 = temp;/*
		vec2_t tuv = uv0;
		uv0 = uv1;
		uv1 = tuv;*/
	}
	
	dcx = 1.0f/(x1-x0);
	
	for(i=x0;i<x1;i++){
		
		uvinterp = (1.0f*(i-x0))*dcx;
		
		u = ((uint32_t)round(Lerp(uv1.x,uv0.x,uvinterp)));
		v = ((uint32_t)round(Lerp(uv1.y,uv0.y,uvinterp)));
		//printf("%d %d\n",u,v);
		u%=tex.xi;
		v%=tex.yi;
		tcol = tex.pixdata[u+v*tex.xi];
		//printf("0x%08X\n",tcol);
		REND_PutPixel(i,y,tcol);
		
	}
}




void REND_DrawTextureTriangle_FBottom(vec3_t v0, vec3_t v1, vec3_t v2,
									  vec2_t uv0, vec2_t uv1, vec2_t uv2,
									  texture_t tex,
									  uint32_t col)
{

	float sl, sr, dsl, dsr; //x left, x right, left interpolant, right interpolant
	//float uvfactl, uvfactr;
	vec2_t uvl, uvr;
	
	uint16_t i = 0, j = 0;
	uint16_t x0, y0, x2, y2; //uint versions of important points
	
	uint32_t tcol; //pixel color
	
	//swap vertices if x's are swapped
	if(v2.x<v1.x){
		
		vec3_t temp = v1;
		v1 = v2;
		v2=temp;
		/*
		vec2_t tempuv = uv1;
		uv1 = uv2;
		uv2=tempuv;
		*/
	}	
	
	y0 = (uint16_t)v0.y; y2 = (uint16_t)v2.y; //assign the y coordinates
	
	//calculate pixel slopes
	sl = v0.x; sr = v0.x;
	dsl = (1.0f*(v0.x-v1.x))/(v0.y-v1.y);
	dsr = (1.0f*(v0.x-v2.x))/(v0.y-v2.y);
	
	float dul, dur, dvl, dvr;
	
	dul = (1.0f*(uv0.x-uv1.x))/(v0.y-v1.y);
	dur = (1.0f*(uv0.x-uv2.x))/(v0.y-v2.y);
	
	dvl = (1.0f*(uv0.y-uv1.y))/(v0.y-v1.y);
	dvr = (1.0f*(uv0.y-uv2.y))/(v0.y-v2.y);	
	
	
	uvl = uv0;
	uvr = uv0;
	
	for(i=y0;i<y2;i++){
		
		x0 = (uint16_t)round(sl);
		x2 = (uint16_t)round(sr);
		/*
		uvfactr = (y2-i)/(v2.y-v0.y);
		uvfactl = (y2-i)/(v2.y-v1.y);
		
		uvl = V2_Lerp(uv2,uv1,uvfactl);
		uvr = V2_Lerp(uv2,uv0,uvfactl);
		*/	
		REND_DrawTextureScanline(x0, x2, i, uvl, uvr, tex, col);

		sl += dsl;
		sr += dsr;
		uvl.x += dul;
		uvr.x += dur;
		uvl.y += dvl;
		uvr.y += dvr;
		
	}

}

void REND_DrawTextureTriangle_FTop( vec3_t v0, vec3_t v1, vec3_t v2,
									vec2_t uv0, vec2_t uv1, vec2_t uv2,
									texture_t tex,
									uint32_t col)
{

	float sl, sr, dsl, dsr; //x left, x right, left interpolant, right interpolant
	//float uvfactl, uvfactr, duvr, duvl;
	vec2_t uvl, uvr;	
	
	uint16_t i = 0, j = 0;
	uint16_t x0, y0, x2, y2; //uint versions of important points
	
	uint32_t tcol; //pixel color

	//swap vertices if x's are swapped
	if(v1.x<v0.x){
		
		vec3_t temp = v1;
		v1 = v0;
		v0=temp;
		/*
		vec2_t tempuv = uv1;
		uv1 = uv0;
		uv0=tempuv;
		*/
	}	
	
	y0 = (uint16_t)v0.y; y2 = (uint16_t)v2.y; //assign the y coordinates
	
	//calculate pixel slopes
	sl = v0.x; sr = v1.x;
	dsl = (1.0f*(v2.x-v0.x))/(v2.y-v0.y);
	dsr = (1.0f*(v2.x-v1.x))/(v2.y-v1.y);
	
	float dul, dur, dvl, dvr;
	
	dul = (1.0f*(uv2.x-uv0.x))/(v2.y-v0.y);
	dur = (1.0f*(uv2.x-uv1.x))/(v2.y-v1.y);
	
	dvl = (1.0f*(uv2.y-uv0.y))/(v2.y-v0.y);
	dvr = (1.0f*(uv2.y-uv1.y))/(v2.y-v1.y);	
	
	uvl = uv0;
	uvr = uv1;
	
	for(i=y0;i<y2;i++){
		
		x0 = (uint16_t)round(sl);
		x2 = (uint16_t)round(sr);
		/*
		uvfactr = (v2.y-i)/(v2.y-v0.y);
		uvfactl = (v2.y-i)/(v2.y-v1.y);
		
		uvl = V2_Lerp(uv2,uv1,uvfactl);
		uvr = V2_Lerp(uv2,uv0,uvfactl);
		*/
		REND_DrawTextureScanline(x0, x2, i, uvl, uvr, tex, col);
		
		sl += dsl;
		sr += dsr;
		uvl.x += dul;
		uvr.x += dur;
		uvl.y += dvl;
		uvr.y += dvr;
	}
	
}


void REND_DrawTextureTriangle(vec3_t v1, vec3_t v2, vec3_t v3, vec2_t uv1, vec2_t uv2, vec2_t uv3, texture_t tex, uint32_t col){
	
	uint8_t i = 0, j = 0;
	
	vec3_t verts[4];
	vec2_t uvs[4];
	
	i = REND_IsVInScreen(v1) & REND_IsVInScreen(v2) & REND_IsVInScreen(v3);
	
	if(i==0) //one of the triangle's vertices isn't on screen
		return; //ignore triangle (cheap solution lol)
	
	verts[0] = v1;  uvs[0] = uv1;
	verts[1] = v2;  uvs[1] = uv2;
	verts[2] = v3;  uvs[2] = uv3;
	
	for(i=0;i<3;i++){
		verts[i].x = (int16_t)round(verts[i].x + (WRENX>>1)); 
		verts[i].y = (int16_t)round(verts[i].y + (WRENY>>1));
		uvs[i].x *= tex.xi-1;
		uvs[i].y *= tex.yi-1;

	}
	//Y Order vertices
	for(i=0;i<2;i++)
		for(j=0;j<2-i;j++)
			if(verts[j].y>verts[j+1].y){
				vec3_t temp = verts[j];
				verts[j] = verts[j+1];
				verts[j+1] = temp;
				vec2_t tempuv = uvs[j];
				uvs[j] = uvs[j+1];
				uvs[j+1] = tempuv; 
			}
				

	
	if(verts[0].y == verts[1].y){
		if(verts[1].x<verts[0].x){
			vec2_t temp = uvs[0];
			uvs[0] = uvs[1];
			uvs[1] = temp;
		}
		REND_DrawTextureTriangle_FTop(verts[0], verts[1], verts[2], uvs[0], uvs[1], uvs[2], tex, col);
		
	}
	
	else if(verts[1].y == verts[2].y){
		if(verts[2].x>verts[1].x){
			vec2_t temp = uvs[2];
			uvs[2] = uvs[1];
			uvs[1] = temp;
		}		
		REND_DrawTextureTriangle_FBottom(verts[0], verts[1], verts[2], uvs[0], uvs[1], uvs[2], tex, col);
	}
	
	else{
		
		verts[3].y = verts[1].y; //calculate split
		//scrverts[3].x = ((scrverts[2].x-scrverts[0].x)*(scrverts[0].y-scrverts[3].y))/(scrverts[0].y-scrverts[2].y)+scrverts[0].x; 
		
		//verts[3].x = ((verts[2].x-verts[0].x)*(verts[0].y-verts[3].y))/(verts[0].y-verts[2].y)+verts[0].x;
		verts[3].x = Lerp(verts[0].x,verts[2].x,(verts[0].y-verts[3].y)/(verts[0].y-verts[2].y));
		
		float vdist = (verts[0].y-verts[3].y)/(verts[0].y-verts[2].y);
		
		//scrverts[3].color = AEON_Lerp_32bpp_Color(scrverts[0].color,scrverts[2].color,(1.0f*scrverts[0].y-scrverts[3].y)/(scrverts[0].y-scrverts[2].y));
		
		uvs[3] = V2_Lerp(uvs[2],uvs[0],vdist);
		
		//printf("%f %f\n",uvs[3].x,uvs[3].y);
		//uvs[3] = uvs[2];
		
		//uvs[3] = uvs[0];
		
		if(verts[3].x>verts[1].x){
			vec2_t temp = uvs[3];
			uvs[3] = uvs[1];
			uvs[1] = temp;
		}
		
		REND_DrawTextureTriangle_FTop(verts[1], verts[3], verts[2],
									  uvs[1], uvs[3], uvs[2],
									  tex, col);
								  
		REND_DrawTextureTriangle_FBottom(verts[0], verts[1], verts[3],
										 uvs[0], uvs[1], uvs[3],
										 tex, col);
										 

	}
	
}
