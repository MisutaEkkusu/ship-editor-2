#ifndef _BMPOPS_H_
#define _BMPOPS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct BMPFILEH{
	
	uint16_t BM; //reserved, has to be "0x424D"
	uint32_t fSize; //BMP file size
	uint32_t reserved; //two 16 bit set to 0
	uint32_t pixDataOffset; //offset to pixel data
	
}__attribute__((packed, aligned(1))) BMPFILEH_t; //14 bytes

typedef struct BMPIMGH{
	
	uint32_t hSize; //header size
	uint32_t w; //image width
	uint32_t h; //image height
	uint16_t bitPlanes; //bitPlanes (1)
	uint16_t bpp; //bits per pixel
	uint32_t bComp; //bit compression (0 for uncompressed)
	uint32_t imgSize; //Image size (bytes)
	uint32_t xPPM; //pixels per meter resolution (X)
	uint32_t yPPM; //pixels per meter resolution (Y)
	uint32_t palColorUsed; //Number of colors in palette
	uint32_t palColorImpo; //Number of Important colors (0 for all)
	
}__attribute__((packed, aligned(1))) BMPIMGH_t; //40 bytes

typedef struct BMPFILE{
	
	BMPFILEH_t fHeader;
	BMPIMGH_t iHeader;
	uint32_t* colData; // color palette data
	uint8_t* pData; // pixel data
	
}__attribute__((packed, aligned(1))) BMPFILE_t;

uint8_t ReadBMP(char* filename, BMPFILE_t* BMP);
void DestroyBMP(BMPFILE_t* BMP);

#endif
