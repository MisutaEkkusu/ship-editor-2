#ifndef _SHIPDEF_H_
#define _SHIPDEF_H_

#include "IMPMATH.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define MAXSTRLEN 256



typedef enum ERRTYPE { NOERR, NOTV2, ALLOC, INOUT, FATAL } ERRTYPE_t;

/*
typedef enum container_typeV1 { WSID, WSTITLE, WSDESCR, WSVISIB, WSTAGS,
								SHPVTX, SHPNRM, SHPUVS, SHPTRI, SHPTEX,
								ENGPOS, LCONTR, RCONTR, ENGCL1, ENGCL2,
								HUDCOL, SETTNG, UNKNWN } container_typeV1_t;
*/								
typedef enum container_type { WSID, WSTITLE, WSDESCR, WSVISIB, WSTAGS, WSPREV,
							  SHPMSH, CPTMSH, SHPTXD, CPTTXD, SHPTXI, CPTTXI,
							  TRPRCY, ENGINS, ENGTOG, TRAISZ, CONTLS, ENGDIM,
							  ENGBRT, HNDSET, SHPPOS, SHPROT, SHPSCL, UNKNWN } container_type_t;
								
typedef struct SHP_container{
	
	char name[MAXSTRLEN];	//container name
	container_type_t type;
	int64_t startAddress;	//container start address
	int32_t size;			//container size	
	uint8_t *data;       	//generic data bytes
	
} SHP_container_t;


typedef struct mesh{
	
	int32_t meshCount;
	int32_t vertLen;
	int32_t normLen;
	int32_t uvsLen;
	int32_t triLen;
	
	vec3_t *verts;
	vec3_t *norms;
	vec2_t *uvs;
	
	int32_t *triidx; //every three is a triangle
	
}mesh_t;

typedef struct wshop_data{
	
	uint64_t wsID;					//ID
	uint64_t wsVis;					//visibility
	char wsTitle[MAXSTRLEN];		//title
	char wsDescription[MAXSTRLEN];	//description
	char wsTags[MAXSTRLEN];			//tags
	
} wshop_data_t;

typedef struct texture{
	
	uint32_t xi;
	uint32_t yi;

	uint32_t *pixdata;
	
}texture_t;

typedef struct SHP_file{
	
	char type[MAXSTRLEN];	//file type (ship)
	int32_t ncontainer; 	//number of containers
	SHP_container_t *container;
	
	wshop_data_t wsData;
	mesh_t shp_msh;
	mesh_t cpt_msh;
	
	texture_t shp_dif;
	texture_t cpt_dif;
	texture_t shp_ilu;
	texture_t cpt_ilu;	
	
	vec3_t pos;
	vec3_t rot;
	vec3_t scl;
	
} SHP_file_t;


void dread(void* output, const void* data, size_t sz, size_t* offset);
uint8_t ShpReadFileMesh(FILE* fp, mesh_t* msh);
uint8_t ReadFileString(FILE* fp, char* outstr);
uint8_t ReadDataString(uint8_t* data, char* outstr);
container_type_t GetContainerType(char* cname);
void ExportMeshOBJ(mesh_t msh, const char* filename);
ERRTYPE_t LoadSHPFile(const char* filename, SHP_file_t* shpF);
ERRTYPE_t CleanSHPFile(SHP_file_t* shpF);

#endif
