#ifndef _MESHRENDER_H_
#define _MESHRENDER_H_

#include "SHIPDEF.h"
#include "SDLFUNC.h"

#define WRENX 320
#define WRENY 200

#define BACKFACE_CULL 	0x00
#define M_WIREFRAME 	0x01
#define M_SHADED		0x02
#define M_TEXTURED		0x03

#define TOP_CLIPCODE 0x01 //0001
#define BOTTOM_CLIPCODE 0x02 //0010
#define RIGHT_CLIPCODE 0x04 //0100
#define LEFT_CLIPCODE 0x08 //1000
#define INSIDE_CLIPCODE 0x00 //0000

uint8_t RenderMode; // render mode register

mat3_t RX, RY, RZ;	//Euler Matrices
mat3_t ID;			//Identity Matrix

vec3_t MDLPOS;	//position vec
vec3_t MDLROT;	//rotation vec
vec3_t MDLSCL;	//scale vec

mat3_t MDLMAT;  //model transform matrix

mesh_t viewMesh;//view mesh (identity mesh is stored in ship file structure [SHIPDEF.h])
texture_t viewMeshTexture; //view Mesh texture

typedef struct tri{
	
	int32_t vi[3];//vertex index
	float zmid;   //z midpoint
	uint32_t zi;  //z place
	
}tri_t;

tri_t* viewMeshTris;

typedef struct cam{
	
	vec3_t dir;
	vec3_t pos;
	mat3_t mat;
	
}cam_t;

cam_t camera;


//register ops
void RegBitSet(uint8_t *reg, uint8_t offs);
void RegBitUnSet(uint8_t *reg, uint8_t offs);
uint8_t RegBitCheck(uint8_t reg, uint8_t offs);

uint8_t MSH_SetViewMesh(mesh_t in);
void MSH_FreeViewMesh();
void MSH_IdentVerts(mesh_t in);

void MSH_Translate(mesh_t* msh, vec3_t trs);	//Translate mesh
void MSH_Rotate(mesh_t* msh, vec3_t rot);		//Euler X->Y->Z rotation or more
void MSH_Scale(mesh_t* msh, vec3_t scl);		//Scale mesh
void MSH_CalcTriZMid();

void REND_SetupRenderEngine();
void REND_PutPixel(uint16_t x, uint16_t y, uint32_t col);
void REND_DrawScanLine(uint16_t x1, uint16_t x2, uint16_t y, uint32_t col);
void REND_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint32_t col);
void REND_DrawFlatColTriangle(vec3_t v1, vec3_t v2, vec3_t v3, uint32_t col);
void REND_DrawWireColTriangle(vec3_t v1, vec3_t v2, vec3_t v3, uint32_t col);
void REND_DrawTextureTriangle(vec3_t v1, vec3_t v2, vec3_t v3, vec2_t uv1, vec2_t uv2, vec2_t uv3, texture_t tex, uint32_t col);
void REND_ZSortTris();
void REND_DrawMesh();

#endif