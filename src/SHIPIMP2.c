#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <png.h>

#include "SHIPDEF.h"
#include "FONT.h"
#include "SDLFUNC.h"
#include "MESHREND.h"
#include "BMPOPS.h"


int main(int argc, char* argv[]){
	
	uint32_t T_tic, T_toc, T_elapsed;
	char msg[20];
	uint32_t i = 0;
	
	camera.pos.x = 0.0f;
	camera.pos.y = 0.0f;
	camera.pos.z = 100.0f;	
	camera.dir.x = 0.0f;
	camera.dir.y = 0.0f;
	camera.dir.z = -1.0f;
	
	SHP_file_t shpF;
	//Tenrai T51-A.shp
	if(LoadSHPFile("DATA/SHIPS/ST010X.shp", &shpF)!=NOERR){
		return EXIT_FAILURE;
	}
	
	if(MSH_SetViewMesh(shpF.shp_msh) == 0)
		return EXIT_FAILURE;
	
	//LOAD BITMAP TEXTURE
	BMPFILE_t bmtex;
	
	if(!ReadBMP("DATA/SHIPS/ST010X.bmp", &bmtex)){
		return EXIT_FAILURE;
	}
	
	viewMeshTexture.xi = bmtex.iHeader.w;
	viewMeshTexture.yi = bmtex.iHeader.h;
	
	viewMeshTexture.pixdata = calloc(viewMeshTexture.xi*viewMeshTexture.yi,sizeof(uint32_t));
	if(viewMeshTexture.pixdata == NULL){
		return EXIT_FAILURE;
	}
	memcpy(viewMeshTexture.pixdata,bmtex.pData,sizeof(uint32_t)*viewMeshTexture.xi*viewMeshTexture.yi);

	//Display code
	
	
	bitfont_width = LoadFonts(&bitfont);
	if(bitfont_width == 0)
		return EXIT_FAILURE;
			
	if(InitGraphics(0,WRENX,WRENY) == 0)
		return EXIT_FAILURE;
	
	SDL_SetWindowFullscreen(window,SDL_WINDOW_FULLSCREEN_DESKTOP);
	SDL_GetWindowSize(window,(int*)&screenX,(int*)&screenY);

	T_tic = SDL_GetTicks(); //initiate clock
	
	MDLSCL.x = 15.0f;
	MDLSCL.y = 15.0f;
	MDLSCL.z = 15.0f;
	
	MDLPOS.x = 0.0f;
	MDLPOS.y = 0.0f;
	MDLPOS.z = 0.0f;
	
	MDLROT.x = 0.0f;
	MDLROT.y = 0.0f;
	MDLROT.z = 0.0f;
	
	REND_SetupRenderEngine();	
	
	vec3_t vtri[3];
	vec2_t uvtri[3];
		
	

	
	while(!mLquit){
		
		
		T_toc = SDL_GetTicks();
		T_elapsed = T_toc-T_tic;
		snprintf(msg,20,"FPS:%0.0f",1000.0f/T_elapsed);
		
		MSH_IdentVerts(shpF.shp_msh);
		
		MDLROT.x = M_PI*cursor.yf+M_PI;
		MDLROT.y = -2*M_PI*cursor.xf+M_PI;
		MDLROT.z = 0.05*M_PI*sin(T_toc*0.001f);
		MSH_Scale(&viewMesh,MDLSCL);
		MSH_Rotate(&viewMesh,MDLROT);
		MSH_Translate(&viewMesh,MDLPOS);	
		MSH_CalcTriZMid();	
		/*
		vtri[0].x = 0; vtri[0].y = 80; vtri[0].z = 0;
		uvtri[0].x = 0.5f; uvtri[0].y = 0.0f;
		
		vtri[1].x = -60; vtri[1].y = -60; vtri[1].z = 0;
		uvtri[1].x = 0.0f; uvtri[1].y = 1.0f;
		
		vtri[2].x = 60; vtri[2].y = -60; vtri[2].z = 0;
		uvtri[2].x = 1.0f; uvtri[2].y = 1.0f;
		
		RZ = M3_Euler(T_toc*0.0005f, 'z');
		
		vtri[0]=V3_MatMul(vtri[0], RZ);
		vtri[1]=V3_MatMul(vtri[1], RZ);
		vtri[2]=V3_MatMul(vtri[2], RZ);
		*/
		if(T_elapsed>33){
			
			T_tic = SDL_GetTicks();
			
			memset(fbuffer,0x00,WRENX*WRENY*sizeof(uint32_t));
			BlitText(fbuffer,WRENX,msg,0,0,0x0000FF00);
			
			BlitText(fbuffer,WRENX,shpF.wsData.wsTitle,0,8,0x0000FF00);
			/*for(i=0;i<200;i++)
				memcpy(fbuffer+i*320,viewMeshTexture.pixdata+i*viewMeshTexture.xi,viewMeshTexture.xi*sizeof(uint32_t));*/
			
			REND_ZSortTris();
			
			REND_DrawMesh();
			
			//REND_DrawTextureTriangle(vtri[0], vtri[1], vtri[2], uvtri[0], uvtri[1], uvtri[2], viewMeshTexture, 0);
			
			UpdateFB(fbuffer,WRENX*sizeof(uint32_t));
			
		}
		ParseInput();
		
	}
	//ExportMeshOBJ(viewMesh, "out.obj");
	puts("Freeing Memory...");
	free(viewMeshTexture.pixdata);
	free(bitfont);
	CloseGraphics();
	MSH_FreeViewMesh();
	CleanSHPFile(&shpF);
	DestroyBMP(&bmtex);
	puts("Exiting...");
	

	return EXIT_SUCCESS;
}
